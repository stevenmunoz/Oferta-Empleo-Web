﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebOfertaEmpleo.Models;

namespace WebOfertaEmpleo.Controllers
{
    public class VacantesController : Controller
    {
        private OfertaEntities db = new OfertaEntities();
        ServiciosDane serviciosWeb = new ServiciosDane();

        public ActionResult Map()
        {
            return View();
        }

        // GET: Vacantes
        public ActionResult Index()
        {
            if (Session["nomUsu"] != null)
            {
                return View(db.Vacante.ToList().Where(m => m.Empleador == Session["nomUsu"].ToString() && m.Estado != "R"));
            }
            else 
            {
                return RedirectToAction("Index", "Home", "");
            }
        }

        // GET: Vacantes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacante vacante = db.Vacante.Find(id);
            if (vacante == null)
            {
                return HttpNotFound();
            }
            return View(vacante);
        }

        // GET: Vacantes/Detalles/5
        public ActionResult Detalles(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacante vacante = db.Vacante.Find(id);
            if (vacante == null)
            {
                return HttpNotFound();
            }

            Departamento dpto = new Departamento();
            dpto.Id = vacante.Departamento;
            dpto.Nombre = db.Departamento.Where(u => u.Id == vacante.Departamento).Select(u => u.Nombre).First();
            vacante.Departamento1 = dpto;

            Municipio municipio = new Municipio();
            municipio.Id = vacante.Municipio;
            municipio.Nombre = db.Municipio.Where(u => u.Id == vacante.Municipio).Select(u => u.Nombre).First();
            vacante.Municipio1 = municipio;

            if (vacante.Tipo == 1)
                vacante.TipoNombre = "Contrato Laboral";
            else if (vacante.Tipo == 2)
                vacante.TipoNombre = "Prestación de Servicios";
            else if (vacante.Tipo == 3)
                vacante.TipoNombre = "Pasantía";
            else if (vacante.Tipo == 4)
                vacante.TipoNombre = "Práctica laboral";
            else if (vacante.Tipo == 5)
                vacante.TipoNombre = "Contrato de aprendizaje";

            if (vacante.Salario == 0)
                vacante.SalarioNombre = "Menos de 1 SMMLV";
            else if (vacante.Salario == 1)
                vacante.SalarioNombre = "1 SMMLV";
            else if (vacante.Salario == 2)
                vacante.SalarioNombre = "Más de 1 SMMLV hasta 2 SMMLV";
            else if (vacante.Salario == 3)
                vacante.SalarioNombre = "Más de 2 SMMLV hasta 3 SMMLV";
            else if (vacante.Salario == 4)
                vacante.SalarioNombre = "Más de 3 SMMLV hasta 5 SMMLV";
            else if (vacante.Salario == 5)
                vacante.SalarioNombre = "Más de 5 SMMLV hasta 7 SMMLV";
            else if (vacante.Salario == 6)
                vacante.SalarioNombre = "Más de 7 SMMLV";
            else if (vacante.Salario == 7)
                vacante.SalarioNombre = "A convenir";

            if (vacante.Experiencia == 0)
                vacante.ExperienciaNombre = "Ninguna";
            else if (vacante.Experiencia == 1)
                vacante.ExperienciaNombre = "De 0 a  6 meses";
            else if (vacante.Experiencia == 2)
                vacante.ExperienciaNombre = "De 7 a 12 meses";
            else if (vacante.Experiencia == 3)
                vacante.ExperienciaNombre = "De 13 a 24 meses";
            else if (vacante.Experiencia == 4)
                vacante.ExperienciaNombre = "Más de 24 meses";

            if (vacante.Nivel_estudios == 1)
                vacante.Nivel_estudiosNombre = "NINGUNO";
            else if (vacante.Nivel_estudios == 2)
                vacante.Nivel_estudiosNombre = "PREESCOLAR";
            else if (vacante.Nivel_estudios == 3)
                vacante.Nivel_estudiosNombre = "BÁSICA PRIMARIA (1 - 5)";
            else if (vacante.Nivel_estudios == 4)
                vacante.Nivel_estudiosNombre = "BÁSICA SECUNDARIA (6 - 9)";
            else if (vacante.Nivel_estudios == 5)
                vacante.Nivel_estudiosNombre = "MEDIA (10 - 13)";
            else if (vacante.Nivel_estudios == 6)
                vacante.Nivel_estudiosNombre = "POSTGRADO";
            else if (vacante.Nivel_estudios == 7)
                vacante.Nivel_estudiosNombre = "TÉCNICA LABORAL";
            else if (vacante.Nivel_estudios == 8)
                vacante.Nivel_estudiosNombre = "TÉCNICA PROFESIONAL";
            else if (vacante.Nivel_estudios == 9)
                vacante.Nivel_estudiosNombre = "TECNOLÓGICA";
            else if (vacante.Nivel_estudios == 10)
                vacante.Nivel_estudiosNombre = "UNIVERSITARIA";
            else if (vacante.Nivel_estudios == 11)
                vacante.Nivel_estudiosNombre = "ESPECIALIZACIÓN";
            else if (vacante.Nivel_estudios == 12)
                vacante.Nivel_estudiosNombre = "MAESTRÍA";
            else if (vacante.Nivel_estudios == 13)
                vacante.Nivel_estudiosNombre = "DOCTORADO";

            return View(vacante);
        }

        /*public JsonResult consumirServicioDepartamentos()
        {
            string url = "http://www.dane.gov.co/Divipola/ControladorDivipola?tipoConsulta=depa";
            List<SelectListItem> selectDpto = new List<SelectListItem>();

            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                WebResponse response = request.GetResponse();

                XmlSerializer serializer = new XmlSerializer(typeof(listaDEPARTAMENTO));
                listaDEPARTAMENTO miListaDptos = null;

                using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    miListaDptos = (listaDEPARTAMENTO)serializer.Deserialize(sr);
                }

                return Json(new { array = miListaDptos.arrayDptos }, JsonRequestBehavior.AllowGet);

            }
            catch (WebException ex)
            {
                String xmlExc = ex.Message;
            }

            return null;
        }*/

        public JsonResult consumirServicioDepartamentos()
        {
            var deptos = from dptos in db.Departamento
                         select new DptoViewModel { Id = dptos.Id, Nombre = dptos.Nombre};

            return Json(new { array = deptos }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consumirServicioMunicipio(int idDpto)
        {
            var munic = from municipios in db.Municipio.Where(m => m.Departamento == idDpto)
                             select new MunicipioViewModel { Id = municipios.Id, Nombre = municipios.Nombre };

            return Json(new { array = munic }, JsonRequestBehavior.AllowGet);
        }
        /*public JsonResult consumirServicioMunicipio(string idDpto)
        {
            string url = "http://www.dane.gov.co/Divipola/ControladorDivipola?tipoConsulta=municpio&idDepa=" + idDpto + "";
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                WebResponse response = request.GetResponse();

                XmlSerializer serializer = new XmlSerializer(typeof(listaMunpio));
                listaMunpio miListaMunicip = null;

                using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    miListaMunicip = (listaMunpio)serializer.Deserialize(sr);
                }

                return Json(new { array = miListaMunicip.arrayMunicipios }, JsonRequestBehavior.AllowGet);

            }
            catch (WebException ex)
            {
                String xmlExc = ex.Message;
            }

            return null;
        }*/

        public JsonResult Busqueda(string palabra, int? tipo, int? salario, int? experiencia, int? nivel, int? municipio, string busqueda)
        {
            var resultado = from context in db.stp_busqueda_vacantes(palabra, tipo, salario, experiencia, nivel, municipio, busqueda)
                            select new VacanteViewModel
                            {
                                ID = context.ID,
                                Municipio = context.Municipio,
                                Departamento = context.Departamento,
                                Num_vacantes = context.Num_vacantes,
                                Cargo = context.Cargo,
                                Titulo = context.Titulo,
                                Descripcion = context.Descripcion,
                                Fecha_publicacion = context.Fecha_publicacion,
                                Fecha_vencimiento = context.Fecha_vencimiento,
                                DiasVence = context.DiasVence,
                                Empleador = context.Empleador,
                                Sector = context.Sector,
                                Profesion = context.Profesion,
                                SalarioTexto = context.Salario,
                                NivelTexto = context.Nivel_estudios,
                                ExperienciaTexto = context.Experiencia,
                                latitud = context.Latitud,
                                longitud = context.Longitud,
                                Indicativo = context.Indicativo,
                                Telefono = context.Telefono,
                                Celular = context.Celular,
                                Direccion = context.Direccion,
                                Email = context.Email
                            };

            return Json(new { vacantes = resultado }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Detalle(int id)
        {
            var resultDetail = (from vacante in db.Vacante
                          where vacante.ID == id
                          select new VacanteViewModel 
                          { 
                              ID = vacante.ID, 
                              Municipio = vacante.Municipio, 
                              Departamento = vacante.Departamento, 
                              Num_vacantes = vacante.Num_vacantes,
                              Cargo = vacante.Cargo,
                              Titulo = vacante.Titulo,
                              Descripcion = vacante.Descripcion,
                              Fecha_publicacion = vacante.Fecha_publicacion,
                              Fecha_vencimiento = vacante.Fecha_vencimiento,
                              Empleador = vacante.Empleador,
                              Sector = vacante.Sector,
                              Profesion = vacante.Profesion,
                              Salario = vacante.Salario,
                              Nivel_estudios = vacante.Nivel_estudios,
                              Experiencia = vacante.Experiencia,
                              latitud = vacante.Latitud,
                              longitud = vacante.Longitud,
                              Indicativo = vacante.Indicativo,
                              Telefono = vacante.Telefono,
                              Celular = vacante.Celular,
                              Direccion = vacante.Direccion,
                              Email = vacante.Email
                          }).FirstOrDefault();

            resultDetail.DiasVence = CantDiasVence(resultDetail.Fecha_publicacion, resultDetail.Fecha_vencimiento);

            return Json(resultDetail, JsonRequestBehavior.AllowGet);
        }

        // GET: Vacantes/Create
        public ActionResult Create()
        {
            crearViewbagSelect();
            return View();
        }

        public void crearViewbagSelect()
        {
            //Viewbag para tipo de oferta
            List<SelectListItem> selectTipo = new List<SelectListItem>();
            selectTipo.Add(new SelectListItem { Text = "Empleo", Value = "1" });
            selectTipo.Add(new SelectListItem { Text = "Pasantía", Value = "2" });
            selectTipo.Add(new SelectListItem { Text = "Práctica laboral", Value = "3" });
            selectTipo.Add(new SelectListItem { Text = "Contrato de aprendizaje", Value = "4" });
            ViewBag.selectTipo = selectTipo;

            //Viewbag para cantidad de trabajadores
            List<SelectListItem> selectNumTrabajadores = new List<SelectListItem>();
            selectNumTrabajadores.Add(new SelectListItem { Text = "1", Value = "1" });
            selectNumTrabajadores.Add(new SelectListItem { Text = "2", Value = "2" });
            selectNumTrabajadores.Add(new SelectListItem { Text = "3", Value = "3" });
            selectNumTrabajadores.Add(new SelectListItem { Text = "4", Value = "4" });
            selectNumTrabajadores.Add(new SelectListItem { Text = "5", Value = "5" });
            ViewBag.selectNumTrabajadores = selectNumTrabajadores;

            //Viewbag para cantidad de trabajadores
            List<SelectListItem> selectSalario = new List<SelectListItem>();
            selectSalario.Add(new SelectListItem { Text = "Cualquiera", Value = "10" });
            selectSalario.Add(new SelectListItem { Text = "Menos de 1 SMMLV", Value = "0" });
            selectSalario.Add(new SelectListItem { Text = "1 SMMLV", Value = "1" });
            selectSalario.Add(new SelectListItem { Text = "1 SMMLV hasta 2 SMMLV", Value = "2" });
            selectSalario.Add(new SelectListItem { Text = "Más de 2 SMMLV hasta 3 SMMLV", Value = "3" });
            selectSalario.Add(new SelectListItem { Text = "Más de 3 SMMLV hasta 5 SMMLV", Value = "4" });
            selectSalario.Add(new SelectListItem { Text = "Más de 5 SMMLV hasta 7 SMMLV", Value = "5" });
            selectSalario.Add(new SelectListItem { Text = "Más de 7 SMMLV", Value = "6" });
            selectSalario.Add(new SelectListItem { Text = "A convenir", Value = "7" });
            ViewBag.selectSalario = selectSalario;

            //Viewbag para experiencia
            List<SelectListItem> selectExperiencia = new List<SelectListItem>();
            selectExperiencia.Add(new SelectListItem { Text = "Sin Experiencia", Value = "0" });
            selectExperiencia.Add(new SelectListItem { Text = "De 0 a 6 meses", Value = "1" });
            selectExperiencia.Add(new SelectListItem { Text = "De 7 a 12 meses", Value = "2" });
            selectExperiencia.Add(new SelectListItem { Text = "De 13 a 24 meses", Value = "3" });
            selectExperiencia.Add(new SelectListItem { Text = "Más de 24 meses", Value = "4" });
            ViewBag.selectExperiencia = selectExperiencia;

            //Viewbag para nivel educativo
            List<SelectListItem> selectNivel = new List<SelectListItem>();
            selectNivel.Add(new SelectListItem { Text = "Cualquiera", Value = "1" });
            selectNivel.Add(new SelectListItem { Text = "Preescolar", Value = "2" });
            selectNivel.Add(new SelectListItem { Text = "Básica Primaria (1 - 5)", Value = "3" });
            selectNivel.Add(new SelectListItem { Text = "Básica Secundaria (6 - 9)", Value = "4" });
            selectNivel.Add(new SelectListItem { Text = "Media (10 - 13)", Value = "5" });
            selectNivel.Add(new SelectListItem { Text = "Postgrado", Value = "6" });
            selectNivel.Add(new SelectListItem { Text = "Técnica Laboral", Value = "7" });
            selectNivel.Add(new SelectListItem { Text = "Técnica Profesional", Value = "8" });
            selectNivel.Add(new SelectListItem { Text = "Tecnológica", Value = "9" });
            selectNivel.Add(new SelectListItem { Text = "Universitaria", Value = "10" });
            selectNivel.Add(new SelectListItem { Text = "Especialización", Value = "11" });
            selectNivel.Add(new SelectListItem { Text = "Maestría", Value = "12" });
            selectNivel.Add(new SelectListItem { Text = "Doctorado", Value = "13" });
            ViewBag.selectNivel = selectNivel;

            //Viewbag para el select de indicativo
            List<SelectListItem> selectIndicativo = new List<SelectListItem>();
            selectIndicativo.Add(new SelectListItem { Text = "+1", Value = "1" });
            selectIndicativo.Add(new SelectListItem { Text = "+2", Value = "2" });
            selectIndicativo.Add(new SelectListItem { Text = "+4", Value = "3" });
            selectIndicativo.Add(new SelectListItem { Text = "+5", Value = "4" });
            selectIndicativo.Add(new SelectListItem { Text = "+6", Value = "5" });
            selectIndicativo.Add(new SelectListItem { Text = "+7", Value = "6" });
            selectIndicativo.Add(new SelectListItem { Text = "+8", Value = "7" });
            ViewBag.selectIndicativo = selectIndicativo;

            //Viewbag para departamentos
            //ViewBag.selectDpto = consumirServicioDptos();
        }

        // POST: Vacantes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Titulo,Tipo,Descripcion,Experiencia,Num_vacantes,Cargo,Fecha_vencimiento,Nivel_estudios,Profesion,Salario,Municipio,Departamento,Latitud,Longitud,Empleador,Busqueda,Num_denuncias,Estado,Direccion,Email,Indicativo,Telefono,Celular")] Vacante vacante)
        {
            if (ModelState.IsValid)
            {
                vacante.Estado = "A";
                vacante.Ultima_Actualizacion = DateTime.Now;
                vacante.Busqueda = vacante.Titulo + " " + vacante.Descripcion + " " + vacante.Cargo + " " + vacante.Profesion;
                vacante.Num_denuncias = 0;
                vacante.Sector = "1";
                TimeSpan ts = new TimeSpan(23, 59, 59);
                vacante.Fecha_publicacion = DateTime.Now.Date + ts;
                vacante.Fecha_vencimiento = vacante.Fecha_vencimiento.Date + ts;
                vacante.Empleador = Session["nomUsu"] != null ? Session["nomUsu"].ToString() : "Servicio de Empleo";
                db.Vacante.Add(vacante);

                try
                {
                    db.SaveChanges();

                    string titulo = "Se ha publicado exitosamente la vacante '" + vacante.Titulo + "' a través del Servicio de Empleo Móvil";
                    
                    string mensaje_enviar = "Señor/a " + vacante.Empleador + "<br/><br/>" +
                          "Acaba de publicar una vacante en el Servicio de Empleo Móvil.<br/><br/>" +
                          "RESUMEN DE LA VACANTE:<br/><br/>" +
                          "Título de la vacante: " + vacante.Titulo + "<br/>" +
                          "Tipo de oportunidad”: " + vacante.Tipo + "<br/>" +
                          "Descripción de la vacante: " + vacante.Descripcion + "<br/>" +
                          "Cargo: " + vacante.Cargo + "<br/>" +
                          "Salario ofrecido: " + vacante.Salario + "<br/>" +
                          "Experiencia mínima requerida: " + vacante.Experiencia + "<br/>" +
                          "Nivel de estudio mínimo requerido”: " + vacante.Nivel_estudios + "<br/>" +
                          "Profesión: " + vacante.Profesion + "<br/>" +
                          "Ubicación: " + vacante.Departamento + "/" + vacante.Municipio + "<br/>" +
                          "Dirección de referencia: " + vacante.Direccion + "<br/>" +
                          "Correo Electrónico de Contacto: " + vacante.Email + "<br/>" +
                          "Teléfono de Contacto: " + vacante.Telefono + "<br/><br/>" +
                          "Servicio de Empleo Móvil - Este es un correo electrónico automático, por favor no lo responda";

                    vacante.Email = "johnedu06@gmail.com";

                    SendMail.Email("servicioempleomovil@gmail.com", "asdf1234QWER", vacante.Email, titulo, mensaje_enviar, "", "Servicio de Empleo Móvil", "0");
                }
                catch(Exception ex)
                {
                    string error = ex.Message;
                }

                return RedirectToAction("Index", "Vacantes", null);
            }

            return View(vacante);
        }

        // GET: Vacantes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Vacante vacante = null;
            
            if (Session["nomUsu"] != null)
            {
                vacante = db.Vacante.Where(m => m.ID == id && m.Empleador == Session["nomUsu"].ToString()).FirstOrDefault();
            }

            if (vacante == null)
            {
                return RedirectToAction("Index", "Home", "");
            }

            crearViewbagSelect();

            return View(vacante);
        }

        // POST: Vacantes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Titulo,Tipo,Descripcion,Experiencia,Num_vacantes,Cargo,Fecha_vencimiento,Nivel_estudios,Profesion,Salario,Municipio,Departamento,Latitud,Longitud,Empleador,Busqueda,Num_denuncias,Estado,Direccion,Email,Indicativo,Telefono,Celular")] Vacante vacante)
        {
            if (ModelState.IsValid)
            {
                vacante.Ultima_Actualizacion = DateTime.Now;
                vacante.Busqueda = vacante.Titulo + " " + vacante.Descripcion + " " + vacante.Cargo + " " + vacante.Profesion;
                TimeSpan ts = new TimeSpan(23, 59, 59);
                vacante.Sector = "1"; 
                vacante.Fecha_publicacion = DateTime.Now.Date + ts;
                vacante.Fecha_vencimiento = vacante.Fecha_vencimiento.Date + ts;
                vacante.Empleador = Session["nomUsu"] != null ? Session["nomUsu"].ToString() : "Servicio de Empleo";
                db.Entry(vacante).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Vacantes", null);
            }
            crearViewbagSelect();
            return View(vacante);
        }

        // GET: Vacantes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacante vacante = db.Vacante.Find(id);
            if (vacante == null)
            {
                return HttpNotFound();
            }
            return View(vacante);
        }

        // POST: Vacantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Vacante vacante = db.Vacante.Find(id);
            vacante.Estado = "R";
            db.Entry(vacante).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public int CantDiasVence(DateTime fechaPublicacion, DateTime fechaVencimiento) 
        {
            double diferencia = (fechaVencimiento - fechaPublicacion).TotalDays;
            int cantidad = (int)diferencia;

            return cantidad;
        }

        public JsonResult denunciarVacante(DenunciaModel denuncia)
        {
            Vacante vacante = db.Vacante.Find(denuncia.vacanteId);

            Denuncias denunciaVacante = new Denuncias();
            denunciaVacante.Fecha = DateTime.Now;
            denunciaVacante.Tipo = denuncia.Tipo;
            denunciaVacante.ID_vacante = denuncia.vacanteId;
            denunciaVacante.Vacante = vacante;

            try
            {
                db.Denuncias.Add(denunciaVacante);
                int n = db.SaveChanges();

                int? cantidad = db.Vacante.Where(m => m.ID == denuncia.vacanteId).Select(m => m.Num_denuncias).FirstOrDefault();
                cantidad++;

                if (cantidad == 3)
                {
                    vacante.Estado = "I";
                    vacante.Num_denuncias = cantidad;

                }
                else
                {
                    vacante.Num_denuncias = cantidad;
                }

                db.Entry(vacante).State = EntityState.Modified;
                db.SaveChanges();

                SendMail.Email("servicioempleomovil@gmail.com", "asdf1234QWER", denuncia.Email, denuncia.TituloEmail, denuncia.TextoEmail, "", "Servicio de Empleo Móvil", "0");

                return Json(new { result = "1" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string mensaje = ex.Message;
                return Json(new { result = "0" }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
