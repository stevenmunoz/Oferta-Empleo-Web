﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebOfertaEmpleo.Models;

namespace WebOfertaEmpleo.Controllers
{
    public class FaqController : Controller
    {
        private OfertaEntities db = new OfertaEntities();
        //
        // GET: /Faq/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Faqs() 
        {
            var listFaqs = db.Preguntas_frecuentes.ToList();

            return Json(new { faqs = listFaqs }, JsonRequestBehavior.AllowGet);
        }
	}
}