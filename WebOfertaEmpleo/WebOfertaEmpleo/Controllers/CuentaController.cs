﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebOfertaEmpleo.Controllers
{
    public class CuentaController : Controller
    {
        // GET: Cuenta
        public ActionResult Index()
        {
            return View();
        }

        public void Login(string usuario) 
        {
            Session["nomUsu"] = usuario;
        }

        public ActionResult Salir() 
        {
            Session["nomUsu"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}