﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebOfertaEmpleo.Models;

namespace WebOfertaEmpleo.Controllers
{
    public class HomeController : Controller
    {
        ServiciosDane serviciosWeb = new ServiciosDane();

        public ActionResult Index()
        {
            //Viewbag para departamentos
            //ViewBag.selectDpto = serviciosWeb.consumirServicioDptos();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}