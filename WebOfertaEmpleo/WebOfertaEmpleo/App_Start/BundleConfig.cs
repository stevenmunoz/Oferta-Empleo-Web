﻿using System.Web;
using System.Web.Optimization;

namespace WebOfertaEmpleo
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

             bundles.Add(new ScriptBundle("~/bundles/scripts-web").Include(
                        "~/Scripts/jquery.js",
                        "~/Scripts/jqueryui.js",
                        "~/Scripts/owl.carousel.min.js",
                        "~/Scripts/jquery.swipebox.js",
                        "~/Scripts/colorbox.js",
                        "~/Scripts/snap.js",
                        "~/Scripts/contact.js",
                        "~/Scripts/custom.js",
                        "~/Scripts/framework.js",
                        "~/Scripts/framework.launcher.js",
                        "~/Scripts/bd.js"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                      "~/styles/style.css",
                      "~/styles/framework.css",
                      "~/styles/owl.carousel.css",
                      "~/styles/owl.theme.css",
                      "~/styles/swipebox.css",
                      "~/styles/colorbox.css"));

            bundles.Add(new StyleBundle("~/bundles/mapstyles").Include(
                    "~/styles/style.css",
                    "~/styles/frameworkmap.css",
                    "~/styles/owl.carousel.css",
                    "~/styles/owl.theme.css",
                    "~/styles/swipebox.css",
                    "~/styles/colorbox.css"));


            // Para la depuración, establezca EnableOptimizations en false. Para obtener más información,
            // visite http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
