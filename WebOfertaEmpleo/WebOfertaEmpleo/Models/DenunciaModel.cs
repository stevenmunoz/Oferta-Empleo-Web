﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebOfertaEmpleo.Models
{
    public class DenunciaModel
    {
        public int vacanteId { get; set; }
        public string Tipo { get; set; }
        public string Email { get; set; }
        public string TituloEmail { get; set; }
        public string TextoEmail { get; set; }
    }
}