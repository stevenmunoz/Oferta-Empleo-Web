﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace WebOfertaEmpleo.Models
{
    public class SendMail
    {
        public static bool Email(string gMailAccount, string password, string to, string subject, string message, string bcc, string DisplayName, string pAttachmentPath)
        {
            try
            {
                NetworkCredential loginInfo = new NetworkCredential(gMailAccount, password);
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(gMailAccount, DisplayName);
                string[] tos = to.Split(',');
                for (int i = 0; i < tos.Length; i++)
                {
                    msg.To.Add(new MailAddress(tos[i]));
                }
                if (bcc != "")
                    msg.Bcc.Add(new MailAddress(bcc));
                msg.Subject = subject;
                msg.Body = message;
                msg.IsBodyHtml = true;

                if (pAttachmentPath.Trim() != "0")
                {
                    msg.Attachments.Add(new Attachment(pAttachmentPath));
                }

                msg.Priority = MailPriority.High;
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = loginInfo;
                client.Send(msg);

                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
        
        //SendMail("servicioempleomovil@gmail.com", "asdf1234QWER", Email, tituloEmail, textoEmail, "", "Servicio de Empleo Móvil", "0");
    }
}