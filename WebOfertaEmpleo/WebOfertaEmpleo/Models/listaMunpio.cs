﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebOfertaEmpleo.Models
{
    public class listaMunpio
    {
        [XmlElement("MUNICIPIO")]
        public Municipio[] arrayMunicipios { get; set; }
    }
}