﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebOfertaEmpleo.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class OfertaEntities : DbContext
    {
        public OfertaEntities()
            : base("name=OfertaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Denuncias> Denuncias { get; set; }
        public virtual DbSet<Preguntas_frecuentes> Preguntas_frecuentes { get; set; }
        public virtual DbSet<Vacante> Vacante { get; set; }
        public virtual DbSet<Departamento> Departamento { get; set; }
        public virtual DbSet<Municipio> Municipio { get; set; }
    
        public virtual ObjectResult<stp_Vacantes_FullTextSearch_Result> stp_Vacantes_FullTextSearch(string palabra_Frase, Nullable<int> tipo, Nullable<int> salario, Nullable<int> experiencia, Nullable<int> nivel, Nullable<int> municipio, string tipoBusqueda)
        {
            var palabra_FraseParameter = palabra_Frase != null ?
                new ObjectParameter("Palabra_Frase", palabra_Frase) :
                new ObjectParameter("Palabra_Frase", typeof(string));
    
            var tipoParameter = tipo.HasValue ?
                new ObjectParameter("Tipo", tipo) :
                new ObjectParameter("Tipo", typeof(int));
    
            var salarioParameter = salario.HasValue ?
                new ObjectParameter("Salario", salario) :
                new ObjectParameter("Salario", typeof(int));
    
            var experienciaParameter = experiencia.HasValue ?
                new ObjectParameter("Experiencia", experiencia) :
                new ObjectParameter("Experiencia", typeof(int));
    
            var nivelParameter = nivel.HasValue ?
                new ObjectParameter("Nivel", nivel) :
                new ObjectParameter("Nivel", typeof(int));
    
            var municipioParameter = municipio.HasValue ?
                new ObjectParameter("Municipio", municipio) :
                new ObjectParameter("Municipio", typeof(int));
    
            var tipoBusquedaParameter = tipoBusqueda != null ?
                new ObjectParameter("TipoBusqueda", tipoBusqueda) :
                new ObjectParameter("TipoBusqueda", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<stp_Vacantes_FullTextSearch_Result>("stp_Vacantes_FullTextSearch", palabra_FraseParameter, tipoParameter, salarioParameter, experienciaParameter, nivelParameter, municipioParameter, tipoBusquedaParameter);
        }
    
        public virtual ObjectResult<stp_busqueda_vacantes_Result> stp_busqueda_vacantes(string palabra_Frase, Nullable<int> tipo, Nullable<int> salario, Nullable<int> experiencia, Nullable<int> nivel, Nullable<int> municipio, string tipoBusqueda)
        {
            var palabra_FraseParameter = palabra_Frase != null ?
                new ObjectParameter("Palabra_Frase", palabra_Frase) :
                new ObjectParameter("Palabra_Frase", typeof(string));
    
            var tipoParameter = tipo.HasValue ?
                new ObjectParameter("Tipo", tipo) :
                new ObjectParameter("Tipo", typeof(int));
    
            var salarioParameter = salario.HasValue ?
                new ObjectParameter("Salario", salario) :
                new ObjectParameter("Salario", typeof(int));
    
            var experienciaParameter = experiencia.HasValue ?
                new ObjectParameter("Experiencia", experiencia) :
                new ObjectParameter("Experiencia", typeof(int));
    
            var nivelParameter = nivel.HasValue ?
                new ObjectParameter("Nivel", nivel) :
                new ObjectParameter("Nivel", typeof(int));
    
            var municipioParameter = municipio.HasValue ?
                new ObjectParameter("Municipio", municipio) :
                new ObjectParameter("Municipio", typeof(int));
    
            var tipoBusquedaParameter = tipoBusqueda != null ?
                new ObjectParameter("TipoBusqueda", tipoBusqueda) :
                new ObjectParameter("TipoBusqueda", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<stp_busqueda_vacantes_Result>("stp_busqueda_vacantes", palabra_FraseParameter, tipoParameter, salarioParameter, experienciaParameter, nivelParameter, municipioParameter, tipoBusquedaParameter);
        }
    }
}
