﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOfertaEmpleo.Models
{
    public class VacanteViewModel
    {
        public long ID { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Titulo { get; set; }

        [Display(Name = "Tipo de Oportunidad")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Tipo { get; set; }

        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Descripcion { get; set; }

        [Display(Name = "Número de Trabajadores Requeridos")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Num_vacantes { get; set; }

        [Display(Name = "Cargo")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Cargo { get; set; }

        [Display(Name = "Salario")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Salario { get; set; }

        [Display(Name = "Sector")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Sector { get; set; }

        [Display(Name = "Experiencia Laboral")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Experiencia { get; set; }

        [Display(Name = "Nivel Educativo")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Nivel_estudios { get; set; }

        [Display(Name = "Profesión")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Profesion { get; set; }

        [Display(Name = "Municipio")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Municipio { get; set; }

        [Display(Name = "Departamento")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public int Departamento { get; set; }

        [Display(Name = "Fecha de Publicación")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public System.DateTime Fecha_publicacion { get; set; }

        [Display(Name = "Fecha de Vencimiento")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public System.DateTime Fecha_vencimiento { get; set; }

        [Display(Name = "Indicativo")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Indicativo { get; set; }

        [Display(Name = "Celular de Contacto")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Celular { get; set; }

        [Display(Name = "Teléfono de Contacto")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Telefono { get; set; }

        [Display(Name = "Dirección de Referencia")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Direccion { get; set; }

        [Display(Name = "Correo Electrónico de Contacto")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Email { get; set; }

        public string Empleador { get; set; }

        public int? DiasVence { get; set; }

        public string SalarioTexto { get; set; }

        public string NivelTexto { get; set; }

        public string ExperienciaTexto { get; set; }

        public string latitud { get; set; }

        public string longitud { get; set; }
    }

}