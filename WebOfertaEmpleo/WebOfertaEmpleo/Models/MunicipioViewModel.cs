﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebOfertaEmpleo.Models
{
    public class MunicipioViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}