
var listData = [];
var markersArray = [];

function log() {
    var Auth = new Object();
    //Credenciales
    //Auth.usr = "hotdals";
    //Auth.pass = "74080832";
    var usr = $("#contactNameField").val();
    var pass = $("#contactEmailField").val();
    if (usr != "" && pass != "") {
        MostrarDivCargando();
        Auth.usr = usr;
        Auth.pass = pass;
        var hash = new Object();
        hash.timestamp = "121212121";
        hash.key = "sha1(md5(usr~pass~timestamp~pkey))";
        var iniciosesion = new Object();
        iniciosesion.Auth = Auth;
        iniciosesion.hash = hash;

        $.ajax({
            url: 'http://redempleo.gov.co/index.php?controlador=servicios&accion=il_auth',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(iniciosesion),
            success: function (data, textStatus, xhr) {
                OcultarDivCargando();
                if (data["status"] == 1) {
                    establecerSesion(usr);
                } else {
                    abrirAlert("Usuario o Contraseña incorrectos");
                    $("#contactEmailField").val("");
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OcultarDivCargando();
                abrirAlert("Ha ocurrido un error. Por favor inténtelo en unos minutos.");
                //alert(errorThrown);
            }
        });
    }

}

function establecerSesion(usuario) {
    $.ajax({
        url: '/Cuenta/Login',
        type: 'GET',
        dataType: 'json',
        contentType: "application/json",
        data: { usuario: usuario },
        success: function (data, textStatus, xhr) {
            OcultarDivCargando();
            abrirAlert("Ingreso Exitoso! Será redirigido al administrador de vacantes");
            document.location = "/Vacantes"
        },
        error: function (xhr, textStatus, errorThrown) {
            OcultarDivCargando();
            abrirAlert("Ha ocurrido un error. Por favor inténtelo en unos minutos.");
            //alert(errorThrown);
        }
    });
}

//Ocultar Div cargando...
function OcultarDivCargando(data) {
    $('#loading').css("display", "none");
}

//Mostrar Div cargando...
function MostrarDivCargando(data) {
    $('#loading').css("display", "block");
}

function cargarMunicipio(idDepartamento) {
    $("#Municipio>option").remove();
    $("#Municipio").append("<option value='0'>Cargando municipio ...</option>");
    var opciones = "";
    var firstItem = "";

    $.getJSON("/Vacantes/consumirServicioMunicipio", { idDpto: $("#Departamento").val() }, function (data) {
        $.each(data.array, function (i, values) {
            opciones += " <option value='" + values.Id + "'>" + values.Nombre + "</option>";
            firstItem = firstItem === "" ? values.Id : firstItem;
        });
        $("#Municipio>option").remove();
        $("#Municipio").append(opciones);
        $("#Municipio").val(firstItem);
        $('input.ui-autocomplete-input:eq(1)').val($('#Municipio option:selected').text());
        crearMapa();
    });
}

function cargarDepartamento(){
    
    $("#Departamento>option").remove();
    $("#Departamento").append("<option value='0'>Cargando departamentos ...</option>");
    var opciones = "";
    var firstItem = "";

    $.getJSON("/Vacantes/consumirServicioDepartamentos", function (data) {
        $.each(data.array, function (i, values) {
            opciones += " <option value='" + values.Id + "'>" + values.Nombre + "</option>";
            firstItem = firstItem === "" ? values.Id : firstItem;
        });
        $("#Departamento>option").remove();
        $("#Departamento").append(opciones);
        $("#Departamento").val(firstItem);
        $('input.ui-autocomplete-input:eq(0)').val($('#Departamento option:selected').text());
        cargarMunicipio();
    });
}

function enviar(opcion) {

    if ($("#Departamento").val() == null || $('input.ui-autocomplete-input:eq(0)').val() == "") {
        alert("Debe seleccionar un departamento.")
    }
    else {
        if ($("#Municipio").val() == null || $('input.ui-autocomplete-input:eq(1)').val() == "") {
            alert("Debe seleccionar un municipio.")
        }
        else {


            if (opcion == 1) {
                cargarOfertas($("#contactNameField").val());
            }
            else {
                cargarVacantesMapa($("#contactNameField").val());
            }
        }
    }
}

function cargarOfertas(palabra) {
    $("#map_canvas").hide();

    var detalle = $("#detalle");
    detalle.empty();

    MostrarDivCargando();
    var vectorConectores = ["a", "ante", "bajo", "con", "contra", "de", "desde", "en", "entre", "hacia", "hasta", "para", "por", "según", "segun", "sin", "sobre", "tras", " ", "  ", "   ", ""];

    if (palabra != "") {
        palabra = palabra.trim();
        var palabrasSeparadas = palabra.split(" ");
        var fraseDefinitiva = "";
        var palabraAuxiliar = "";

        for (x = 0; x < palabrasSeparadas.length; x++) {
            palabraAuxiliar = palabrasSeparadas[x];
            palabrasSeparadas[x] = palabraAuxiliar.trim().toLowerCase();
        }

        fraseDefinitiva += palabrasSeparadas[0];

        for (x = 1; x < palabrasSeparadas.length; x++) {
            if ($.inArray(palabrasSeparadas[x], vectorConectores) == -1) {
                fraseDefinitiva += " AND ";
                fraseDefinitiva += palabrasSeparadas[x];
            }
        }

        palabra = fraseDefinitiva;
    }

    var texto = "";
    var ofertas = $("#ofertas");
    ofertas.empty();

    var tipo = $("#selectTipoOportunidad").val();
    var salario = $("#selectSalario").val();
    var experiencia = $("#selectExperiencia").val();
    var nivel = $("#selectNivel").val();
    var municipio = $("#Municipio").val();
    var nom_mun = $("#Municipio option:selected").text();
    var nom_sal = $("#selectSalario option:selected").text();
    var nom_exp = $("#selectExperiencia option:selected").val();
    var nom_niv = $("#selectNivel option:selected").text();
    var nom_dpto = $("#Departamento option:selected").text();
   
    $.ajax({
        url: '/Vacantes/Busqueda',
        type: 'GET',
        data: { palabra: palabra, tipo: tipo, salario: salario, experiencia: experiencia, nivel: nivel, municipio: municipio, busqueda: "N" },
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            var cantidad = data.vacantes.length;
            if (cantidad == 0) {
                abrirAlert("No existen vacantes con los filtros seleccionados, intente seleccionando valores diferentes.");
            }

            texto += '<h2>Se han encontrado ' + cantidad + ' oportunidades</h2>'
            $.each(data.vacantes, function (i, val) {

                var metodoDenuncia = 'GuardarDenuncia(' + val['ID'] + ',\'' + val['Titulo'] + '\',\'' + val['Tipo'] + '\',\'' + val['Descripcion'] + '\',\'' + val['Num_vacantes'] + '\',\'' + val['Cargo'] + '\',\'' + nom_sal + '\',\'' + val['Sector'] + '\',\'' + nom_exp + '\',\'' + val['Nivel_estudios'] + '\',\'' + val['Profesion'] + '\',\'' + nom_dpto + '\',\'' + nom_mun + '\',\'' + val['Fecha_publicacion'] + '\',\'' + val['Fecha_vencimiento'] + '\',\'' + val['DiasVence'] + '\',\'' + val['Empleador'] + '\',\'' + val['Telefono'] + '\',\'' + val['Indicativo'] + '\',\'' + val['Celular'] + '\',\'' + val['Direccion'] + '\',\'' + val['Email'] + '\',\'' + val['Ultima_Actualizacion'] + '\')';

                texto += '<div class="container">' +
                        '<div class="toggle-2">' +
                            '<a href="#" class="deploy-toggle-2 toggle-2">' +
                                val['Titulo'] + '<label style="font-weight: bolder; font-size: 15px; color: black;">';
                if (val['DiasVence'] == 1)
                {
                    texto += 'Vence HOY</label>';
                } else {
                    texto += 'Vence en '+val['DiasVence']+' días</label>';
                }
                texto += '</a>' +
                        '<div class="toggle-content">' +
                              '<p style="text-align:justify;">' +
                                val['Descripcion'] +

                                '<label>' +
                                    'Número de vacantes: <b>' + val['Num_vacantes'] + '</b></label>' +
                                '<label>' +
                                    'Cargo: <b>' + val['Cargo'] + '</b></label>' +
                                '<label>' +
                                    'Salario: <b>' + nom_sal + '</b></label>' +
                                '<label>' +
                                    'Sector: <b>' + val['SalarioTexto'] + '</b></label>' +
                                '<label>' +
                                    'Experiencia: <b>' + val['ExperienciaTexto'] + '</b></label>' +
                                '<label>' +
                                    'Nivel de Estudios: <b>' + val['NivelTexto'] + '</b></label>' +
                                '<label>' +
                                    'Profesión: <b>' + val['Profesion'] + '</b></label>' +
                                '<label>' +
                                    'Departamento: <b>' + nom_dpto + '</b></label>' +
                                '<label>' +
                                    'Municipio: <b>' + nom_mun + '</b></label>' +
                                '<label>' +
                                    'Fecha Vencimiento: <b>' + ToJavaScriptDate(val['Fecha_vencimiento']) + '</b></label>' +
                            '</p>' +

                            '<div class="toggle-content">' +
                               '<p><strong>Datos del Empleador:</strong></p>' +
                                '<div class="one-half-responsive ">' +
                                    '<div class="submenu-navigation">' +
                                         '<div class="submenu-nav-items" style="overflow: hidden; display: block;"></div>' +
                                        '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                            '<ul style="margin-bottom:0px;" class="icon-list">' +
                                                '<li class="right-list">Teléfono (Indicativo): <b>' + val['Telefono'] + ' (' + val['Indicativo'] + ')</b></li>' +
                                            '</ul>' +
                                        '</a>' +
                                       '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                            '<ul style="margin-bottom:0px;" class="icon-list">' +
                                                '<li class="right-list">Celular: <b>' + val['Celular'] + '</b></li>' +
                                            '</ul>' +
                                        '</a>' +
                                        '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                            '<ul style="margin-bottom:0px;" class="icon-list">' +
                                                '<li class="right-list">Dirección: <b>' + val['Direccion'] + '</b></li>' +
                                            '</ul>' +
                                        '</a>' +
                                        '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                            '<ul style="margin-bottom:0px;" class="icon-list">' +
                                                '<li class="right-list">E-mail: <b>' + val['Email'] + '</b></li>' +
                                            '</ul>' +
                                        '</a>' +
                                       '<a name="#" style="text-align:center !important; border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                        'Comparta esta oportunidad de trabajo'+
                                            '<ul style="margin-bottom:0px;" class="icon-list">' +
                                                '<li style="padding-left:30px !important; padding-top:10px;">' +
                                                    '<img src="images/misc/facebook.png" style="margin: 0px !important;" class="star" onclick="abrirPaginaFacebook(\''+val['Titulo']+'\', '+val['ID']+')"/>' +
                                                    '<img src="images/misc/twitter.png" class="star" onclick="abrirPaginaTwitter(\'' + val['Titulo'] + '\', ' + val['ID'] + ')"/>' +
                                                '</li>' +
                                            '</ul>' +
                                        '</a>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="one-half-responsive" style="text-align:center !important;">' +
                                     '<div id="btnDen' + val['ID'] + '" style="padding-left: 40px; width: 80%; float: left;margin: 10px; display:block;"><a name="#" onclick="Denunciar(' + val['ID'] + ')" class="button-icon icon-setting button-red">Denunciar</a></div>' +
                                     '<div id="comboDen' + val['ID'] + '" style="padding-left: 15px; width: 90%; float: left;margin: 10px; display:none;">Motivo de la denuncia: <br />' +
                                     '<select class="styled-select" style="width:100% !important;" name="selectMotivoDenuncia' + val['ID'] + '" id="selectMotivoDenuncia' + val['ID'] + '">' +
                                        '<option value="1">Vacante sospechosa / engañosa</option>' +
                                        '<option value="2">Lenguaje no adecuado</option>' +
                                        '<option value="3">Información de contacto errónea </option>' +
                                        '<option value="4">Sospecha de Trata de personas</option>' +
                                    '</select>' +
                                    '<br /> <a name="#" onclick=\"' + metodoDenuncia + '\" class="button-icon icon-setting button-red">Confirmar denuncia</a>&nbsp;<a name="#" onclick="CancelarDenuncia(' + val['ID'] + ')" class="button-icon icon-setting button-red">Cancelar</a>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '</div>';
            });
            $("#ofertas").html(texto);

            $('.deploy-toggle-2').click(function () {
                $(this).parent().find('.toggle-content').toggle(100);
                $(this).toggleClass('toggle-2-active');
                return false;
            });
            $("#map_canvas").hide();
            OcultarDivCargando();
        },
        error: function (xhr, textStatus, errorThrown) {
            abrirAlert("Ha ocurrido un problema, inténtelo nuevamente.");
            OcultarDivCargando();
        }
    });
}

function cargarVacantesMapa(palabra) {

    var ofertas = $("#ofertas");
    ofertas.empty();

    MostrarDivCargando();
    var vectorConectores = ["a", "ante", "bajo", "con", "contra", "de", "desde", "en", "entre", "hacia", "hasta", "para", "por", "según", "segun", "sin", "sobre", "tras", " ", "  ", "   ", ""];

    if (palabra != "") {
        palabra = palabra.trim();
        var palabrasSeparadas = palabra.split(" ");
        var fraseDefinitiva = "";
        var palabraAuxiliar = "";

        for (x = 0; x < palabrasSeparadas.length; x++) {
            palabraAuxiliar = palabrasSeparadas[x];
            palabrasSeparadas[x] = palabraAuxiliar.trim().toLowerCase();
        }

        fraseDefinitiva += palabrasSeparadas[0];

        for (x = 1; x < palabrasSeparadas.length; x++) {
            if ($.inArray(palabrasSeparadas[x], vectorConectores) == -1) {
                fraseDefinitiva += " AND ";
                fraseDefinitiva += palabrasSeparadas[x];
            }
        }

        palabra = fraseDefinitiva;
    }

    var texto = "";
    var ofertas = $("#ofertas");
    ofertas.empty();

    var tipo = $("#selectTipoOportunidad").val();
    var salario = $("#selectSalario").val();
    var experiencia = $("#selectExperiencia").val();
    var nivel = $("#selectNivel").val();
    var municipio = $("#Municipio").val();

    $.ajax({
        url: '/Vacantes/Busqueda',
        type: 'GET',
        data: { palabra: palabra, tipo: tipo, salario: salario, experiencia: experiencia, nivel: nivel, municipio: municipio, busqueda: "N" },
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            var cantidad = data.vacantes.length;

            if (cantidad != 0) {
                $.each(data.vacantes, function (i, val) {
                    listData[i] = { "ID": "" + val['ID'] + "", "Nombre": "" + val['Titulo'] + "", "GeoLat": "" + val['latitud'] + "", "GeoLong": "" + val['longitud'] + "" };
                });
                setTimeout(function () {
                    Initialize(listData);
                }, 500);
                $("#map_canvas").show();
            }
            else {
                abrirAlertMap("No existen vacantes con los filtros seleccionados, intente seleccionando valores diferentes.");
            }

            OcultarDivCargando();
        },
        error: function (xhr, textStatus, errorThrown) {
            abrirAlert("Ha ocurrido un problema, inténtelo nuevamente.");
            OcultarDivCargando();
        }
    });
}

function Initialize(data) {

    google.maps.visualRefresh = true;

    var city = new google.maps.LatLng(data[0].GeoLat, data[0].GeoLong);

    // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
    var mapOptions = {
        zoom: 14,
        center: city,
        mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    // This makes the div with id "map_canvas" a google map
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    // Using the JQuery "each" selector to iterate through the JSON list and drop marker pins
    $.each(data, function (i, item) {
        var marker = new google.maps.Marker({
            'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
            'map': map,
            'title': item.PlaceName
        });

        // Make the marker-pin blue!
        marker.setIcon('/images/marker.png');

        // put in some information about each json object - in this case, the opening hours.
        var infowindow = new google.maps.InfoWindow({
            content: "<div class='infoDiv'><h2>" + item.Nombre + "</h2><div><input type='submit' class='buttonWrap button button-dark contactSubmitButton' onclick='cargarVacante(" + item.ID + ")' value='Ver detalles' /></div></div>"
        });

        // finally hook up an "OnClick" listener to the map so it pops up out info-window when the marker-pin is clicked!
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });

    })
}

function placeMarker(location) {
    // first remove all markers if there are any
    deleteOverlays();

    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    marker.setIcon('/images/marker.png');

    // add marker in markers array
    markersArray.push(marker);
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
        markersArray.length = 0;
    }
}

function cargarVacante(vacanteID) {

    var texto = "";
    var detalle = $("#detalle");
    detalle.empty();

    var nom_dpto = $("#Departamento option:selected").text();
    var nom_mun = $("#Municipio option:selected").text();
    var nom_sal = $("#selectSalario option:selected").text();
    var nom_exp = $("#selectExperiencia option:selected").val();
    var nom_niv = $("#selectNivel option:selected").text();
    var vacantesGuardadas = localStorage.getItem("vacantesGuardadas");
    var n = 0;

    $.ajax({
        url: '/Vacantes/Detalle',
        type: 'GET',
        data: { id: vacanteID },
        dataType: 'json',
        success: function (data, textStatus, xhr) {

            var metodoDenuncia = 'GuardarDenuncia(' + data['ID'] + ',\'' + data['Titulo'] + '\',\'' + data['Tipo'] + '\',\'' + data['Descripcion'] + '\',\'' + data['Num_vacantes'] + '\',\'' + data['Cargo'] + '\',\'' + nom_sal + '\',\'' + data['Sector'] + '\',\'' + nom_exp + '\',\'' + nom_niv + '\',\'' + data['Profesion'] + '\',\'' + nom_dpto + '\',\'' + nom_mun + '\',\'' + data['Fecha_publicacion'] + '\',\'' + data['Fecha_vencimiento'] + '\',\'' + data['DiasVence'] + '\',\'' + data['Empleador'] + '\',\'' + data['Telefono'] + '\',\'' + data['Indicativo'] + '\',\'' + data['Celular'] + '\',\'' + data['Direccion'] + '\',\'' + data['Email'] + '\',\'' + data['Ultima_Actualizacion'] + '\')';
            n = data['Fecha_vencimiento'].indexOf('T');
            texto += '<div class="container">' +
                    '<div class="toggle-2">' +
                        '<a href="#" class="deploy-toggle-2 toggle-2-active" style="font-weight: normal; font-size: 15px; color: black;">' +
                            data['Titulo'] + '<label style="font-weight: bolder; font-size: 13px; color: black;">';
            if (data['DiasVence'] == 1)
                texto += 'Vence HOY</label>';
            else
                texto += 'Vence en ' + data['DiasVence'] + ' días</label>';
            texto += '</a>' +
                    '<div class="toggle-content" style="overflow: hidden; display: block;">' +
                        '<p style="text-align:justify;">' +
                            '<label style="padding-bottom:10px;">' +
                                data['Descripcion'] +
                            '</label>' +
                            '<label>' +
                                'Número de vacantes: <b>' + data['Num_vacantes'] + '</b></label>' +
                            '<label>' +
                                'Cargo: <b>' + data['Cargo'] + '</b></label>' +
                            '<label>' +
                                'Salario: <b>' + nom_sal + '</b></label>' +
                            '<label>' +
                                'Sector: <b>' + data['Sector'] + '</b></label>' +
                            '<label>' +
                                'Experiencia: <b>' + nom_exp + '</b></label>' +
                            '<label>' +
                                'Nivel de Estudios: <b>' + nom_niv + '</b></label>' +
                            '<label>' +
                                'Profesión: <b>' + data['Profesion'] + '</b></label>' +
                            '<label>' +
                                'Departamento: <b>' + nom_dpto + '</b></label>' +
                            '<label>' +
                                'Municipio: <b>' + nom_mun + '</b></label>' +
                            '<label>' +
                                'Fecha Vencimiento: <b>' + ToJavaScriptDate(data['Fecha_vencimiento']) + '</b></label>' +
                        '</p>' +
                        '<div class="toggle-content" style="overflow: hidden; display: block;">' +
                            '<p><strong>DATOS DE CONTACTO DEL EMPLEADOR:</strong></p>' +
                            '<div class="one-half-responsive ">' +
                                '<div class="submenu-navigation">' +
                                    '<div class="submenu-nav-items" style="overflow: hidden; display: block;"></div>' +
                                    '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                        '<ul style="margin-bottom:0px;" class="icon-list">' +
                                            '<li class="right-list">Teléfono (Indicativo): <b>' + data['Telefono'] + ' (' + data['Indicativo'] + ')</b></li>' +
                                        '</ul>' +
                                    '</a>' +
                                    '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                        '<ul style="margin-bottom:0px;" class="icon-list">' +
                                            '<li class="right-list">Celular: <b>' + data['Celular'] + '</b></li>' +
                                        '</ul>' +
                                    '</a>' +
                                    '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                        '<ul style="margin-bottom:0px;" class="icon-list">' +
                                            '<li class="right-list">Dirección: <b>' + data['Direccion'] + '</b></li>' +
                                        '</ul>' +
                                    '</a>' +
                                    '<a name="#" style="border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                        '<ul style="margin-bottom:0px;" class="icon-list">' +
                                            '<li class="right-list">E-mail: <b>' + data['Email'] + '</b></li>' +
                                        '</ul>' +
                                    '</a>' +
                                    '<a name="#" style="text-align:center !important; border-top: solid 1px rgba(0,0,0,0.1); padding-left: 20px !important; padding-top: 10px !important; padding-bottom: 10px !important; border-bottom: solid 1px rgba(0,0,0,0.1) !important;">' +
                                        'Comparta esta oportunidad de trabajo' +
                                            '<ul style="margin-bottom:0px;" class="icon-list">' +
                                                '<li style="padding-left:30px !important; padding-top:10px;">' +
                                                    '<img src="images/misc/facebook.png" style="margin: 0px !important;" class="star" onclick="abrirPaginaFacebook(\'' + data['Titulo'] + '\', ' + data['ID'] + ')"/>' +
                                                    '<img src="images/misc/twitter.png" class="star" onclick="abrirPaginaTwitter(\'' + data['Titulo'] + '\', ' + data['ID'] + ')"/>' +
                                                '</li>' +
                                            '</ul>' +
                                        '</a>' +
                                '</div>' +
                            '</div>' +
                            '<div class="one-half-responsive" style="text-align:center !important;">' +
                                     '<div id="btnDen' + data['ID'] + '" style="padding-left: 40px; width: 80%; float: left;margin: 10px; display:block;"><a name="#" onclick="Denunciar(' + data['ID'] + ')" class="button-icon icon-setting button-red">Denunciar</a></div>' +
                                     '<div id="comboDen' + data['ID'] + '" style="padding-left: 15px; width: 90%; float: left;margin: 10px; display:none;">Motivo de la denuncia: <br />' +
                                     '<select class="styled-select" style="width:100% !important;" name="selectMotivoDenuncia' + data['ID'] + '" id="selectMotivoDenuncia' + data['ID'] + '">' +
                                        '<option value="1">Vacante sospechosa / engañosa</option>' +
                                        '<option value="2">Lenguaje no adecuado</option>' +
                                        '<option value="3">Información de contacto errónea </option>' +
                                        '<option value="4">Sospecha de Trata de personas</option>' +
                                    '</select>' +
                                    '<br /> <a name="#" onclick=\"' + metodoDenuncia + '\" class="button-icon icon-setting button-red">Confirmar denuncia</a>&nbsp;<a name="#" onclick="CancelarDenuncia(' + data['ID'] + ')" class="button-icon icon-setting button-red">Cancelar</a>' +
                                    '</div>' +
                                '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="formSubmitButtonErrorsWrap">' +
                    '<br />' +
                    '<input type="submit" class="buttonWrap button button-dark contactSubmitButton" id="mapButton" value="Volver al mapa" data-formid="contactForm" onclick="volverMapa();" />' +
                '</div>' +
            '</div>' +
            '</div>';

            $("#detalle").html(texto);

            $('.deploy-toggle-2').click(function () {
                $(this).parent().find('.toggle-content').toggle(100);
                $(this).toggleClass('toggle-2-active');
                return false;
            });

            $("#map_canvas").hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function volverMapa() {
    var detalle = $("#detalle");
    detalle.empty();
    $("#map_canvas").show();
}

function abrirAlert(contenido) {

    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var ancho = windowWidth - (windowWidth / 10);
    $('#content-alert').html('<p>' + contenido + '</p>');
    $("#div-confirm").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: 'Advertencia',
        minWidth: ancho,
        my: "center",
        at: "center",
        of: window,
        show: 'blind',
        hide: 'blind',
        dialogClass: 'prueba',
        buttons: {
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function abrirAlertMap(contenido) {

    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var ancho = windowWidth - (windowWidth / 10);
    $('#content-alert').html('<p>' + contenido + '</p>');
    $("#div-confirm").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: 'Advertencia',
        minWidth: ancho,
        my: "center",
        at: "center",
        of: window,
        show: 'blind',
        hide: 'blind',
        dialogClass: 'prueba',
        buttons: {
            "Aceptar": function () {
                $(this).dialog("close");
                cargarOfertas("");
            }
        }
    });
}

function Denunciar(id) {
    $("#btnDen" + id).hide();
    $("#comboDen" + id).show();
    //alert("Denuncia: "+id)
}

function GuardarDenuncia(id, titulo, tipo, descripcion, vacantes, cargo, salario, sector, experiencia, nivel, profesion, departamento, municipio, fechaPublicacion, fechaVencimiento, diasVence, empleador, telefono, indicativo, celular, direccion, email, fecha_actualizacion) {

    var denuncia = new Object();
    denuncia.Fecha = null;
    denuncia.Tipo = $("#selectMotivoDenuncia" + id + " option:selected").html();
    denuncia.vacanteID = id;
    denuncia.Email = email;
    denuncia.TituloEmail = "La vacante '" + titulo + "' publicada a través del Servicio de Empleo Móvil ha sido denunciada";
    denuncia.TextoEmail = "Señor/a " + empleador + "<br/><br/>" +
          "La vacante '" + titulo + "' ha sido denunciada por varios usuarios de la app. Por precaución la vacante ha sido automáticamente despublicada.<br/><br/>" +
          "RESUMEN DE LA VACANTE:<br/><br/>" +
          "Título de la vacante: " + titulo + "<br/>" +
          "Tipo de oportunidad”: " + tipo + "<br/>" +
          "Descripción de la vacante: " + descripcion + "<br/>" +
          "Cargo: " + cargo + "<br/>" +
          "Salario ofrecido: " + salario + "<br/>" +
          "Experiencia mínima requerida: " + experiencia + "<br/>" +
          "Nivel de estudio mínimo requerido: " + nivel + "<br/>" +
          "Profesión: " + profesion + "<br/>" +
          "Ubicación: " + departamento + "/" + municipio + "<br/>" +
          "Dirección de referencia: " + direccion + "<br/>" +
          "Correo Electrónico de Contacto: " + email + "<br/>" +
          "Teléfono de Contacto: " + telefono + "<br/><br/>" +
          "Servicio de Empleo Móvil - Este es un correo electrónico automático, por favor no lo responda";

    $.ajax({
        url: '/Vacantes/denunciarVacante',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(denuncia),
        success: function (data, textStatus, xhr) {
            if (data.result == "1") {
                $("#btnDen" + id).show();
                $("#comboDen" + id).hide();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            abrirAlert("Ha ocurrido un problema, inténtelo nuevamente.");
            //alert(errorThrown);
        }
    });
}

function CancelarDenuncia(id) {
    $("#btnDen" + id).show();
    $("#comboDen" + id).hide();
}

function abrirPaginaFacebook(nombre, id) {
    var url = 'http://empleomovil.apphb.com/Vacantes/Details/' + id;
    var title = 'Comparta esta vacante';
    var descr = 'Descripción de la vacante de prueba';
    var image = 'http://goo.gl/B8AWrE';
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=auto,left=auto,toolbar=0,status=0,width=auto,height=auto');
}

function abrirPaginaTwitter(nombre, id) {
    var url = 'http://empleomovil.apphb.com/Vacantes/Details/' + id;
    window.open("https://twitter.com/intent/tweet?url=" + url + "&text=Oportunidad de empleo: " + nombre, "_blank", "closebuttoncaption=Regresar");
}

function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}